/**
 * 1 - за допомогою createElement або insertAdjacentHTML. createElement ми спочатку повинні зберегти в змінну, 
 * а потім десь використати. insertAdjacentHTML ми одразу пишемо куди і в якому місці треба вставити, без збереження в змінну.
 * 
 * 2 - перший параматр insertAdjacentHTML це - position. Тобто де відмалювати елемент відносно елемента, який викликав метод.
 * Є 4 позиції: 'beforebegin', 'afterbegin', 'beforeend', 'afterend'. За назвою зрозуміле розташування елемента.
 * 
 * 3 - elem.remove();
 */

const ol = document.createElement('ol');
const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

const showList = (arr, parent = document.body) => {
    arr.forEach(elem => {
        const li = document.createElement('li');
        li.innerText = elem;
        ol.append(li);
    });
    parent.prepend(ol);
};

showList(arr, document.querySelector('.container'));
